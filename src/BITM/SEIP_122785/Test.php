<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 2:04 PM
 */

namespace App;



    trait Helo {
    public function sayHello(){
        echo 'Hello';
    }
}

    trait World{
        public function sayWorld(){
            echo 'World';
        }
    }

class Test{
    use Helo, World;
    public  function sayExclamationMark(){
        echo '!';
    }
}
