<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 3:06 PM
 */

namespace App;

interface canFly{
    public  function fly();
}
interface canSwim{
    public function swim();
}
class Bird
{
    public function info(){
        echo "I am a Bird, My name is $this->name<br>";

    }

    public function describeBird($bird){


    }


}

class Dove extends Bird implements canFly{

    public $name= "Dove";
    public function fly();
    echo "I can Fly <br>";

}
class Penguin extends Bird implements canSwim {

    public  $name= "Penguin";
    public function swim();

    echo "I can swim <br>";
}

class Duck extends Bird implements canFly, canSwim{

    public $nmae="Duck";
    public  function fly()
    {
        // TODO: Implement fly() method.
        echo "I can fly <br>";
    }
    public function swim()
    {
        // TODO: Implement swim() method.
        echo"I can swim <br>";
    }
}