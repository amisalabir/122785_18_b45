<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 2:43 PM
 */

namespace App;


Abstract class AbstractClass
{
// Force Extending class to define this method
    abstract protected function getValue();
    abstract protected function prefixValue($prefix);

    // Common method
    public function printOut() {
        print $this->getValue() . "\n";
    }
}

class ConcreteClass1 extends AbstractClass
{
    protected function getValue() {
        return "ConcreteClass1";
    }

    public function prefixValue($prefix) {
        return "{$prefix}ConcreteClass1";
    }
}

class ConcreteClass2 extends AbstractClass
{
    // Force Extending class to define this method
    abstract protected function getMyValue();
    abstract protected function setMyvalue($myValue);

    // Common method
    public function printOut() {
        print $this->getMyValue(). "\n";
    }

}